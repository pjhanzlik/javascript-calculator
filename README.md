This calculator is made as coursework for freeCodeCamp's Front End Libraries
Developer Certification. In addition to fulfilling
[required user stories](https://learn.freecodecamp.org/front-end-libraries/front-end-libraries-projects/build-a-javascript-calculator),
I wanted to emulate the 
[example freeCodeCamp web page](https://codepen.io/freeCodeCamp/full/wgGVVX)
layout using only [Bootstrap](https://getbootstrap.com/) styling.

Probably the most interesting bit of this project is how
[Redux](https://redux.js.org/)
actions couple intuitively with aria accessible 
[React Bootstrap](https://react-bootstrap.github.io/) components.  Unfortunately,
Bootstrap themes, which are flexbox based, do not scale to 
[240px width devices](https://www.kaiostech.com/meet-the-devices-that-are-powered-by-kaios/)
by default. [Bulma](https://bulma.io/) themes, which are CSS grid based,
could make the app more responsive by default, but that would mean sacrificing
some built-in Bootstrap accessibility features.

This site has been manually tested to render nicely on mobile and desktop
[WebKit](https://webkit.org/), [Blink](https://www.chromium.org/blink), and
[Servo](https://servo.org/) powered browsers, but your milage may vary.
If you do spot a rendering problem, please
[create a new Issue](https://gitlab.com/pjhanzlik/javascript-calculator/issues)
with a screenshot of the problem and a description of your hardware.

