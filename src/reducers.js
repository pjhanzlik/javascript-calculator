import {
  CLEAR,
  EVALUATE,
  NUMBER,
  OPERATOR
} from './actions';

const INIT = {
  input: '0',
  sign: '',
  queue: []
};

function calc(lop, op, rop) {
  const LOP = Number.parseFloat(lop);
  const ROP = Number.parseFloat(rop);
  switch(op) {
    case '*':
      return LOP * ROP;
    case '/':
      return LOP / ROP;
    case '+':
      return LOP + ROP;
    case '-':
      return LOP - ROP;
    default:
      throw new Error(`Unsupported operation: ${op}`);
  }
}

function calculator(state = INIT, action) {
  switch (action.type) {
    case CLEAR:
      return INIT;
    case NUMBER:
      if (state.input === '0' && action.value !== '.') {
        return {
          ...state,
          input: action.value
        };
      }
      else if (action.value === '.' && state.input.includes('.')) {
        return { ...state };
      }
      else {
        return {
          ...state,
          input: state.input + action.value
        }
      }
    case OPERATOR:
      if (state.input === INIT.input) {
        if (action.value === '-') {
          if (state.sign === '-') {
            return {
              ...state,
              sign: ''
            }
          }
          else {
            return {
              ...state,
              sign: '-'
            }
          }
        }
        // ignore non-negative operations as first input from INIT
        else if (state.queue === INIT.queue) {
          return state;
        }
        else {
          return {
            ...INIT,
            queue: state.queue.slice(0, state.queue.length - 1).concat([action.value])
          }
        }
      }
      else {
        return {
          ...INIT,
          queue: state.queue.concat([state.sign + state.input, action.value])
        }
      }
    case EVALUATE:
      const EQ = state.queue.concat(state.sign + state.input);
      if(EQ.length === 1) {
        return {...INIT, input: EQ[0]};
      }
      else {
        while(EQ.length >= 5) {
          if(EQ[3] === '*' || EQ[3] === '/') {
            EQ.splice(2,3,calc(EQ[2],EQ[3],EQ[4]));
          }
          else {
            EQ.splice(0,3,calc(EQ[0],EQ[1],EQ[2]));
          }
        }
        return {...INIT, input: calc(EQ[0], EQ[1], EQ[2])};
      }
    default:
      return state;
  }
}

export default calculator;