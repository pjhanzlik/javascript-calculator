export const CLEAR = 'CLEAR';
export const EVALUATE = 'EVALUATE';
export const NUMBER = 'NUMBER';
export const OPERATOR = 'OPERATOR';

export function clear() {
    return { 'type': CLEAR };
}
export function evaluate() {
    return { 'type': EVALUATE };
}
export function number(value) {
    return {
        'type': NUMBER,
        'value': `${value}`
    }
}
export function operator(value) {
    return {
        'type': OPERATOR,
        'value': `${value}`
    }
}