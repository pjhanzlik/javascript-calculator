import React from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";

import Display from "./containers/Display";
import ClearButton from "./containers/ClearButton";
import NumberButton from "./containers/NumberButton";
import OperationButton from "./containers/OperationButton";
import EvaluateButton from "./containers/EvaluateButton";

import './App.css';

function App(props) {
  return (
    <Card bg="info">
      <Card.Header>
        <Display id="display" />
      </Card.Header>
      <Card.Body>
        <Container fluid>
          <Row>
            <Col xs={6}>
              <ClearButton
                block
                value="A/C"
                id="clear"
              />
            </Col>
            <Col>
              <OperationButton
                block
                value="/"
                id="divide"
              />
            </Col>
            <Col>
              <OperationButton
                block
                value="*"
                id="multiply"
              />
            </Col>
          </Row>
          <Row>
            <Col>
              <NumberButton
                variant="dark"
                block
                value={7}
                id="seven"
              />
            </Col>
            <Col>
              <NumberButton
                variant="dark"
                block
                value={8}
                id="eight"
              />
            </Col>
            <Col>
              <NumberButton
                variant="dark"
                block
                value={9}
                id="nine"
              />
            </Col>
            <Col>
              <OperationButton
                block
                value="-"
                id="subtract"
              />
            </Col>
          </Row>
          <Row>
            <Col>
              <NumberButton
                variant="dark"
                block
                value={4}
                id="four"
              />
            </Col>
            <Col>
              <NumberButton
                variant="dark"
                block
                value={5}
                id="five"
              />
            </Col>
            <Col>
              <NumberButton
                variant="dark"
                block
                value={6}
                id="six"
              />
            </Col>
            <Col>
              <OperationButton
                block
                value="+"
                id="add"
              />
            </Col>
          </Row>
          <Row>
            <Col xs={9}>
              <Row>
                <Col>
                  <NumberButton
                    variant="dark"
                    block
                    value={1}
                    id="one"
                  />
                </Col>
                <Col>
                  <NumberButton
                    variant="dark"
                    block
                    value={2}
                    id="two"
                  />
                </Col>
                <Col>
                  <NumberButton
                    variant="dark"
                    block
                    value={3}
                    id="three"
                  />
                </Col>
              </Row>
              <Row>
                <Col xs={8}>
                  <NumberButton
                    variant="dark"
                    block
                    value={0}
                    id="zero"
                  />
                </Col>
                <Col>
                  <NumberButton
                    variant="dark"
                    block
                    value="."
                    id="decimal"
                  />
                </Col>
              </Row>
            </Col>
            <Col>
              <EvaluateButton
                block
                value="="
                id="equals"
              />
            </Col>
          </Row>
        </Container>
      </Card.Body>
    </Card>
  );
}

export default App;
