import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import calculatorApp from './reducers'
import App from './App.jsx';

import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';

const STORE = createStore(
    calculatorApp,   
    window.__REDUX_DEVTOOLS_EXTENSION__ &&
    window.__REDUX_DEVTOOLS_EXTENSION__()
);
const APP = React.createElement(App);
const PROVIDER = React.createElement(Provider, {store: STORE}, APP);


render(PROVIDER, document.getElementById('root'));

// Uncomment the following segment to run FCC tests
// const TEST = document.createElement("script");
// TEST.setAttribute('src', 'https://cdn.freecodecamp.org/testable-projects-fcc/v1/bundle.js');
// document.body.appendChild(TEST);