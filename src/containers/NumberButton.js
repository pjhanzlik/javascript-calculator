import { connect } from 'react-redux';
import { number } from '../actions';
import InputButton from '../components/InputButton';

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onClick: () => dispatch(number(ownProps.value))
    };
}

const NumberButton = connect(null, mapDispatchToProps)(InputButton);

export default NumberButton;