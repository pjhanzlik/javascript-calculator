import { connect } from 'react-redux';
import Jumbotron from 'react-bootstrap/Jumbotron';

function mapStateToProps(state) {
    return { children: state.input };
}

// mapDispateToProps = {} to supress React div invalid prop warning
const Display = connect(mapStateToProps, {})(Jumbotron);

export default Display;