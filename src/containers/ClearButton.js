import { connect } from 'react-redux';
import { clear } from '../actions';
import ResetButton from '../components/ResetButton';

const mapDispatchToProps = dispatch => {
    return {
        onClick: () => dispatch(clear())
    }
}

const ClearButton = connect(null, mapDispatchToProps)(ResetButton);

export default ClearButton;