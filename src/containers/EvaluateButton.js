import { connect } from 'react-redux';
import { evaluate } from '../actions';
import SubmitButton from '../components/SubmitButton';

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onClick: () => dispatch(evaluate())
    }
}

const EvaluateButton = connect(null, mapDispatchToProps)(SubmitButton);

export default EvaluateButton;