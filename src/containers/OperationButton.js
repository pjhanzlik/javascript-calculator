import { connect } from 'react-redux';
import { operator } from '../actions';
import InputButton from '../components/InputButton';

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onClick: () => dispatch(operator(ownProps.value))
    }
}

const OperationButton = connect(null, mapDispatchToProps)(InputButton);

export default OperationButton;