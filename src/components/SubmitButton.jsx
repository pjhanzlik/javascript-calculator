import React from 'react';
import Button from 'react-bootstrap/Button';

function SubmitButton(props) {
    return (
        <Button {...props}>
            {props.value}
        </Button>
    );
}

SubmitButton.defaultProps = {
    // as: "input",
    // type: "submit",
    variant: "primary",
    value: "Submit",
    readOnly: true
};

export default SubmitButton;