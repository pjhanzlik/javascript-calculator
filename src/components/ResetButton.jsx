import React from 'react';
import Button from 'react-bootstrap/Button';

function ResetButton(props) {
    return (
        <Button {...props}>
            {props.value}
        </Button>
    );
}

ResetButton.defaultProps = {
    // as: "input",
    // type: "reset",
    variant: "danger",
    value: "Reset",
    readOnly: true
};

export default ResetButton;