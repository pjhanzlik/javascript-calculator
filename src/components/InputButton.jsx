import React from 'react';
import Button from 'react-bootstrap/Button';

function InputButton(props) {
    return (
        <Button {...props}>
            {props.value}
        </Button>
    );
}

InputButton.defaultProps = {
    // as: "input",
    // type: "button",
    variant: "secondary",
    readOnly: true
}

export default InputButton;